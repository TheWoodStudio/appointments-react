import React, { Component } from 'react';
import Appointment from './Appointment';
import PropTypes from 'prop-types';

class AppointmentsList extends Component {
    render() { 
        const message = Object.keys(this.props.appointments).length === 0 ? 'No appointments': 'Appointments List';

        return ( 
            <div className="list">
                <h2>{message}</h2>
                {Object.keys(this.props.appointments).map(appointment => (
                    <Appointment
                        key={appointment}
                        info={this.props.appointments[appointment]}
                        removeAppointment={this.props.removeAppointment}
                    />
                ))}
            </div>
         );
    }
}

AppointmentsList.propTypes = {
    appointments: PropTypes.array.isRequired,
    removeAppointment: PropTypes.func.isRequired
}
 
export default AppointmentsList;