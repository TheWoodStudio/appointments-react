import React, { Component } from 'react';
import uuid from 'uuid';
import PropTypes from 'prop-types';

class Form extends Component {

    petName = React.createRef();
    petOwner = React.createRef();
    appointmentDate = React.createRef();
    appointmentTime = React.createRef();
    petSympthoms = React.createRef();

    state = {
        error: false
    }
    
    addNewAppointment = (e) => {
        e.preventDefault();

        const pet = this.petName.current.value;
        const owner = this.petOwner.current.value;
        const date = this.appointmentDate.current.value;
        const time = this.appointmentTime.current.value;
        const sympthoms = this.petSympthoms.current.value;

        //Validate all fields are filled
        if (pet === '' || owner === '' || date === '' || time === '' || sympthoms === '') {
            this.setState({
                error: true
            });
        } else {
            const newAppointment = {
                id: uuid(),
                pet: pet,
                owner: owner,
                date: date,
                time: time,
                sympthoms: sympthoms
            }

            this.props.addAppointment(newAppointment);

            e.currentTarget.reset();

            this.setState({
                error: false
            });
        }
    }

    render() { 
        const error = this.state.error;

        return ( 
            <form onSubmit={this.addNewAppointment}>
                <h2>Add appointment</h2>
                <div className="group">
                    <label>Pet Name</label>
                    <input type="text" ref={this.petName}/>
                </div>
                <div className="group">
                    <label>Pet Owner</label>
                    <input type="text" ref={this.petOwner}/>
                </div>
                <div className="group">
                    <label>Date</label>
                    <input type="date" ref={this.appointmentDate}/>
                </div>
                <div className="group">
                    <label>Time</label>
                    <input type="time" ref={this.appointmentTime}/>
                </div>
                <div className="group">
                    <label>Sympthoms</label>
                    <input type="textarea" ref={this.petSympthoms}/>
                </div>
                <button type="submit">Add</button>
                
                { error ? <p className="error">All fields must be filled</p> : "" }
            </form>

         );
    }
}

Form.propTypes = {
    addAppointment: PropTypes.func.isRequired
}
 
export default Form; 