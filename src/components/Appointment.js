import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Appointment extends Component {

    deleteAppointment = () => {
        this.props.removeAppointment(this.props.info.id);
    }

    render() { 
        const { pet, owner, date, time, sympthoms } = this.props.info;
        return ( 
            <div className="list-item container">
                <div className="column">
                    <h5>{pet}</h5>
                    <ul>
                        <li><span>Pet owner: </span>{owner}</li>
                        <li><span>Date: </span>{date}</li>
                        <li><span>Time: </span>{time}</li>
                        <li><span>Sympthoms: </span>{sympthoms}</li>
                    </ul>
                </div>
                <div className="column">
                    <button onClick={this.deleteAppointment}>Delete</button>
                </div>
            </div>
         );
    }
}

Appointment.propType = {
    info: PropTypes.shape({
        pet: PropTypes.string.isRequired,
        owner: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        time: PropTypes.string.isRequired,
        sympthoms: PropTypes.string.isRequired,        
    }),
    removeAppointment: PropTypes.func.isRequired
}
 
export default Appointment;