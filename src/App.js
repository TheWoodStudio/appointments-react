import React, { Component } from 'react';
import Header from './components/Header';
import Form from './components/Form';
import AppointmentList from './components/AppointmentsList';

class App extends Component {

  state = {
    appointments: [ ]
  }

  componentDidMount() {
    const appointmentsLocal = localStorage.getItem('appointments');
    if (appointmentsLocal) {
      this.setState({
        appointments: JSON.parse(appointmentsLocal)
      });
    }
  }

  componentDidUpdate() {
    localStorage.setItem(
      'appointments',
      JSON.stringify(this.state.appointments)
    );
  }

  addAppointment = (newAppointment) => {
    const appointments = [...this.state.appointments, newAppointment];

    this.setState({
      appointments: appointments
    });
  }

  removeAppointment = (id) => {
    //Get current state copy
    const currentAppointments = [...this.state.appointments];

    //Erase appointment from state
    const appointments = currentAppointments.filter(appointment => appointment.id !== id);

    //Update state
    this.setState({
      appointments: appointments
    });

  }

  render() {
    return (
      <div className="App">
        <Header
          title={"Appointments"}
        />
        <div className="container">
          <div className="column">
            <Form
              addAppointment={this.addAppointment}
            />
          </div>
          <div className="column">
            <AppointmentList
              removeAppointment={this.removeAppointment}
              appointments={this.state.appointments}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
